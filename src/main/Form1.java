package main;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.wb.swt.SWTResourceManager;

import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import socket.CupptSocket;
import thread.ObservableListener;
import observer.Observateur;

public class Form1 {

	protected Shell shlCupptSdk;
	protected Display display;

	private Text textCUPPSPLT, textCUPPSPN, textCUPPSOPR, textCUPPSCN, textCUPPSPP, textXSDU,
			textCUPPSUN, textCUPPSACN;

	private StyledText styledTextGoals, styledTextXML, styledTextEvent, styledTextXMLValidate;

	private Button btnInterfacelevelsavailablerequest, btnConnection, btnInterfacelevelrequest,
			btnObsoleteInterfacelevelrequest, btnUnsupportedInterfacelevelrequest,
			btnCuptestInstancesRequest;

	private TabFolder tabFolder;
	private TabItem tabPageLesson1, tabPageLesson2, tabPageLesson3;

	private Table tableEvents, tableInterfaceLevels, tableDeviceList;

	private Label lblDeviceList;
	private Button btnAuthenticateRequest;

	private Text textDeviceToken;
	private Label lblDeviceToken;
	private TableColumn tableColumn;
	private TableColumn tableColumn_1;
	private TableColumn tableColumn_2;
	private TableColumn tableColumn_3;
	private TableColumn tableColumn_4;
	private TableColumn tableColumn_5;
	private TableColumn tableColumn_6;
	private TabItem tabPageLesson20;
	private Composite compositeLesson20;
	private StyledText styledTextByeResponse;
	private Label lblByeResponse;
	private Button btnDisconnect;

	private CupptSocket platformSocket = new CupptSocket();

	// Vars
	String AirlineCode = "FR";

	String ApplicationCode = "EAS2012CUPPT0007";
	String ApplicationName = "Sample_DCS-J03";
	String ApplicationVersion = "01.03";
	String ApplicationData = "";

	// XSD informations
	String InterfaceLevel;
	String XSDLocalPath;

	String XSDVersion;

	// colors

	Color green = SWTResourceManager.getColor(50, 205, 50);
	Color orange = SWTResourceManager.getColor(255, 165, 0);
	Color red = SWTResourceManager.getColor(SWT.COLOR_RED);

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Form1 window = new Form1();
			window.open();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlCupptSdk.open();
		shlCupptSdk.layout();
		while (!shlCupptSdk.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();

			}
		}

	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlCupptSdk = new Shell();
		shlCupptSdk.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {

				System.out.println("closing...");
				disconnect();

			}
		});

		shlCupptSdk.setImage(SWTResourceManager.getImage(Form1.class, "/main/icon53.png"));
		shlCupptSdk.setMinimumSize(new Point(971, 649));
		shlCupptSdk.setSize(1024, 649);
		shlCupptSdk.setText("CUPPT SDK - lesson 03 Java");

		Group grpPlatformConnection = new Group(shlCupptSdk, SWT.NONE);
		grpPlatformConnection.setText("Platform Connection");
		grpPlatformConnection.setBounds(10, 10, 988, 105);

		Label lblSitecuppsplt = new Label(grpPlatformConnection, SWT.NONE);
		lblSitecuppsplt.setBounds(10, 22, 101, 15);
		lblSitecuppsplt.setText("Site (CUPPSPLT)");

		textCUPPSPLT = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPLT.setBounds(117, 19, 89, 21);

		Label lblNodecuppspn = new Label(grpPlatformConnection, SWT.NONE);
		lblNodecuppspn.setText("Node (CUPPSPN)");
		lblNodecuppspn.setBounds(212, 22, 101, 15);

		textCUPPSPN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPN.setBounds(319, 19, 89, 21);

		Label lblOpratorcuppsopr = new Label(grpPlatformConnection, SWT.NONE);
		lblOpratorcuppsopr.setText("Operator (CUPPSOPR)");
		lblOpratorcuppsopr.setBounds(455, 22, 123, 15);

		textCUPPSOPR = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSOPR.setBounds(584, 19, 89, 21);

		Label lblComputerNamecuppscn = new Label(grpPlatformConnection, SWT.NONE);
		lblComputerNamecuppscn.setText("Computer Name (CUPPSCN)");
		lblComputerNamecuppscn.setBounds(704, 22, 160, 15);

		textCUPPSCN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSCN.setBounds(870, 19, 108, 21);

		Label lblPortcuppspp = new Label(grpPlatformConnection, SWT.NONE);
		lblPortcuppspp.setText("Port (CUPPSPP)");
		lblPortcuppspp.setBounds(212, 49, 101, 15);

		textCUPPSPP = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPP.setBounds(319, 46, 89, 21);

		Label lblXsdSchema = new Label(grpPlatformConnection, SWT.NONE);
		lblXsdSchema.setText("XSD");
		lblXsdSchema.setBounds(212, 77, 101, 15);

		textXSDU = new Text(grpPlatformConnection, SWT.BORDER);
		textXSDU.setBounds(319, 74, 354, 21);

		Label lblUsernamecuppsun = new Label(grpPlatformConnection, SWT.NONE);
		lblUsernamecuppsun.setText("UserName (CUPPSUN)");
		lblUsernamecuppsun.setBounds(455, 49, 123, 15);

		textCUPPSUN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSUN.setBounds(584, 46, 89, 21);

		Label lblAlternateNamecuppsacn = new Label(grpPlatformConnection, SWT.NONE);
		lblAlternateNamecuppsacn.setText("Alternate Name (CUPPSACN)");
		lblAlternateNamecuppsacn.setBounds(704, 49, 160, 15);

		textCUPPSACN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSACN.setBounds(870, 46, 108, 21);

		btnCuptestInstancesRequest = new Button(grpPlatformConnection, SWT.NONE);
		btnCuptestInstancesRequest.setBounds(781, 72, 197, 25);
		btnCuptestInstancesRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnCuptestInstancesRequest_Click(e);
			}
		});
		btnCuptestInstancesRequest.setText("cuptestInstancesRequest");

		tabFolder = new TabFolder(shlCupptSdk, SWT.NONE);
		tabFolder.setBounds(10, 121, 711, 336);

		tabPageLesson1 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson1.setText("Lesson 1");

		Composite compositeLesson1 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson1.setControl(compositeLesson1);

		btnConnection = new Button(compositeLesson1, SWT.NONE);
		btnConnection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonConnect_Click(e);
			}
		});
		btnConnection.setAlignment(SWT.LEFT);

		btnConnection.setBounds(496, 36, 197, 25);
		btnConnection.setText("1 - Network Socket Connection");

		btnInterfacelevelsavailablerequest = new Button(compositeLesson1, SWT.NONE);
		btnInterfacelevelsavailablerequest.setEnabled(false);
		btnInterfacelevelsavailablerequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonInterfacesLevelAvailableRequest_Click(e);
			}
		});
		btnInterfacelevelsavailablerequest.setAlignment(SWT.LEFT);
		btnInterfacelevelsavailablerequest.setText("2 - InterfaceLevelsAvailableRequest");
		btnInterfacelevelsavailablerequest.setBounds(496, 67, 197, 25);

		styledTextXML = new StyledText(compositeLesson1, SWT.BORDER);
		styledTextXML.setBounds(10, 36, 480, 100);
		styledTextXML.setWordWrap(true);

		styledTextEvent = new StyledText(compositeLesson1, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		styledTextEvent.setBounds(10, 142, 683, 156);
		styledTextEvent.setWordWrap(true);
		styledTextEvent.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				styledTextEvent.setTopIndex(styledTextEvent.getLineCount() - 1);
			}
		});

		Label label = new Label(compositeLesson1, SWT.NONE);
		label.setText("XML Message");
		label.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		label.setAlignment(SWT.CENTER);
		label.setBounds(10, 15, 427, 15);

		tabPageLesson2 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson2.setText("Lesson 2");

		Composite compositeLesson2 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson2.setControl(compositeLesson2);

		Label lblNewLabel = new Label(compositeLesson2, SWT.NONE);
		lblNewLabel.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblNewLabel.setBounds(10, 10, 187, 15);
		lblNewLabel.setText("Interface Levels Available List");

		tableInterfaceLevels = new Table(compositeLesson2, SWT.BORDER | SWT.FULL_SELECTION);
		tableInterfaceLevels.setBounds(10, 27, 480, 120);
		tableInterfaceLevels.setHeaderVisible(true);
		tableInterfaceLevels.setLinesVisible(true);

		TableColumn tblclmnLevel = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tblclmnLevel.setWidth(73);
		tblclmnLevel.setText("Level");

		TableColumn tblclmnLocalpath = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tblclmnLocalpath.setWidth(284);
		tblclmnLocalpath.setText("Local Path");

		TableColumn tblclmnXSDVersion = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tblclmnXSDVersion.setWidth(119);
		tblclmnXSDVersion.setText("XSD Version");

		btnInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnInterfacelevelrequest.setEnabled(false);
		btnInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonInterfacesLevelRequest_Click(e);
			}
		});
		btnInterfacelevelrequest.setText("3 - InterfaceLevelRequest");
		btnInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnInterfacelevelrequest.setBounds(496, 27, 197, 25);

		Label lblXmlMessageResponse = new Label(compositeLesson2, SWT.NONE);
		lblXmlMessageResponse.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblXmlMessageResponse.setText("XML Message Response Validation");
		lblXmlMessageResponse.setBounds(10, 153, 187, 15);

		styledTextXMLValidate = new StyledText(compositeLesson2, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		styledTextXMLValidate.setBounds(10, 170, 683, 130);
		styledTextXMLValidate.setWordWrap(true);

		btnObsoleteInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnObsoleteInterfacelevelrequest.setEnabled(false);
		btnObsoleteInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonObsoleteInterfacesLevelRequest_Click(e);
			}
		});
		btnObsoleteInterfacelevelrequest.setForeground(SWTResourceManager.getColor(255, 0, 0));
		btnObsoleteInterfacelevelrequest.setBackground(SWTResourceManager.getColor(255, 0, 0));
		btnObsoleteInterfacelevelrequest.setText("Obsolete InterfaceLevelRequest");
		btnObsoleteInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnObsoleteInterfacelevelrequest.setBounds(496, 122, 197, 25);

		btnUnsupportedInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnUnsupportedInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnUnsuportedInterfacelevelrequest_Click(e);
			}
		});
		btnUnsupportedInterfacelevelrequest.setText("Unsupported InterfaceLevelRequest");
		btnUnsupportedInterfacelevelrequest.setForeground(SWTResourceManager.getColor(255, 0, 0));
		btnUnsupportedInterfacelevelrequest.setEnabled(false);
		btnUnsupportedInterfacelevelrequest.setBackground(SWTResourceManager.getColor(255, 0, 0));
		btnUnsupportedInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnUnsupportedInterfacelevelrequest.setBounds(496, 91, 197, 25);

		Label lblNewLabel_1 = new Label(compositeLesson2, SWT.NONE);
		lblNewLabel_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblNewLabel_1.setBounds(496, 70, 197, 15);
		lblNewLabel_1.setText("InterfaceLevelErrorEvent");

		tabPageLesson3 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson3.setText("Lesson 3");

		Composite compositeLesson3 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson3.setControl(compositeLesson3);

		tableDeviceList = new Table(compositeLesson3, SWT.BORDER | SWT.FULL_SELECTION);
		tableDeviceList.setLinesVisible(true);
		tableDeviceList.setHeaderVisible(true);
		tableDeviceList.setBounds(10, 36, 535, 262);

		tableColumn = new TableColumn(tableDeviceList, SWT.NONE);
		tableColumn.setWidth(120);
		tableColumn.setText("HostName");

		tableColumn_1 = new TableColumn(tableDeviceList, SWT.NONE);
		tableColumn_1.setWidth(110);
		tableColumn_1.setText("DeviceName");

		tableColumn_2 = new TableColumn(tableDeviceList, SWT.NONE);
		tableColumn_2.setWidth(62);
		tableColumn_2.setText("IP");

		tableColumn_3 = new TableColumn(tableDeviceList, SWT.NONE);
		tableColumn_3.setWidth(48);
		tableColumn_3.setText("Port");

		tableColumn_4 = new TableColumn(tableDeviceList, SWT.NONE);
		tableColumn_4.setWidth(69);
		tableColumn_4.setText("Vendor");

		tableColumn_5 = new TableColumn(tableDeviceList, SWT.NONE);
		tableColumn_5.setWidth(55);
		tableColumn_5.setText("Model");

		tableColumn_6 = new TableColumn(tableDeviceList, SWT.NONE);
		tableColumn_6.setWidth(67);
		tableColumn_6.setText("Status");

		lblDeviceList = new Label(compositeLesson3, SWT.NONE);
		lblDeviceList.setText("Device List");
		lblDeviceList.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblDeviceList.setBounds(10, 15, 187, 15);

		btnAuthenticateRequest = new Button(compositeLesson3, SWT.NONE);
		btnAuthenticateRequest.setEnabled(false);
		btnAuthenticateRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnAuthenticateRequest_Click(e);
			}
		});
		btnAuthenticateRequest.setText("4 - Authenticate Request");
		btnAuthenticateRequest.setAlignment(SWT.LEFT);
		btnAuthenticateRequest.setBounds(551, 36, 142, 25);

		textDeviceToken = new Text(compositeLesson3, SWT.BORDER);
		textDeviceToken.setEditable(false);
		textDeviceToken.setBounds(551, 12, 142, 21);

		lblDeviceToken = new Label(compositeLesson3, SWT.NONE);
		lblDeviceToken.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.BOLD));
		lblDeviceToken.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblDeviceToken.setText("Device Token");
		lblDeviceToken.setBounds(421, 7, 124, 21);

		tabPageLesson20 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson20.setText("Lesson 20");

		compositeLesson20 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson20.setControl(compositeLesson20);

		Button btnByeRequest = new Button(compositeLesson20, SWT.NONE);
		btnByeRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnByeRequest_Click(e);
			}
		});
		btnByeRequest.setBounds(496, 31, 197, 25);
		btnByeRequest.setText("Bye Request");

		styledTextByeResponse = new StyledText(compositeLesson20, SWT.BORDER);
		styledTextByeResponse.setWordWrap(true);
		styledTextByeResponse.setBounds(10, 31, 480, 100);

		lblByeResponse = new Label(compositeLesson20, SWT.NONE);
		lblByeResponse.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblByeResponse.setBounds(10, 10, 100, 15);
		lblByeResponse.setText("byeResponse");

		btnDisconnect = new Button(compositeLesson20, SWT.NONE);
		btnDisconnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDisconnect_Click(e);
			}
		});
		btnDisconnect.setText("Disconnect");
		btnDisconnect.setBounds(496, 62, 197, 25);

		tableEvents = new Table(shlCupptSdk, SWT.BORDER | SWT.FULL_SELECTION);
		tableEvents.setBounds(10, 463, 711, 138);
		tableEvents.setHeaderVisible(true);
		tableEvents.setLinesVisible(true);

		TableColumn tblclmnTime = new TableColumn(tableEvents, SWT.NONE);
		tblclmnTime.setWidth(156);
		tblclmnTime.setText("Time");

		TableColumn tblclmnEvent = new TableColumn(tableEvents, SWT.NONE);
		tblclmnEvent.setWidth(550);
		tblclmnEvent.setText("Event");

		Group grpLessonGoals = new Group(shlCupptSdk, SWT.NONE);
		grpLessonGoals.setText("Lesson Goals");
		grpLessonGoals.setBounds(727, 137, 271, 464);

		styledTextGoals = new StyledText(grpLessonGoals, SWT.BORDER);
		styledTextGoals.setBackground(SWTResourceManager.getColor(255, 255, 204));
		styledTextGoals.setBounds(10, 23, 251, 431);
		styledTextGoals.setWordWrap(true);

		loadEnvironmentVariables();
		loadGoals();

		btnCuptestInstancesRequest.setEnabled(false);
	}

	// load Environment Variables
	private void loadEnvironmentVariables() {
		try {
			textCUPPSPN.setText(System.getenv("CUPPSPN"));
			textCUPPSPP.setText(System.getenv("CUPPSPP"));

			textCUPPSPLT.setText(System.getenv("CUPPSPLT"));
			textCUPPSCN.setText(System.getenv("CUPPSCN"));
			textCUPPSACN.setText(System.getenv("CUPPSACN"));

			textCUPPSOPR.setText(System.getenv("CUPPSOPR"));
			textCUPPSUN.setText(System.getenv("CUPPSUN"));

			textXSDU.setText(System.getenv("CUPPSXSDU"));
		} catch (Exception ec) {
			tableEventAddLine("Error getting Environment variables:" + ec);
		}

	}

	// load text goals
	private void loadGoals() {
		styledTextGoals
				.append("1. Sending the <authenticateRequest> message with the application parameters \n");
		styledTextGoals.append("\n");
		styledTextGoals
				.append("2. Getting the device token and the device list by parsing the <authenticateResponse> message \n");
		styledTextGoals.append("\n");
		styledTextGoals.append("3. Parsing the list of devices handled by the platform \n");

	}

	private void buttonConnect_Click(SelectionEvent e) {

		try {
			if (!platformSocket.getSocket().isConnected() || platformSocket.getSocket().isClosed()) {

				String IP = textCUPPSPN.getText();

				int port = Integer.parseInt(textCUPPSPP.getText());

				platformSocket = new CupptSocket(IP, port, "platform");

				tableEventAddLine("Connected to " + IP + ":" + port);
				btnCuptestInstancesRequest.setEnabled(true);

				// Init ObservableListener
				final ObservableListener observablePlatformListener = new ObservableListener(
						platformSocket);

				platformSocket.setObservableListener(observablePlatformListener);

				// Add observator

				platformSocket.getObservableListener().addObservateur(new Observateur() {
					public void update(String message) {

						final String XML = message;

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {

								CupptSocket socket = observablePlatformListener.getSocket();

								System.out.println();

								styledTextEventAppend('R', XML);

								tableEventAddLine("Message " + socket.getIP() + ":" + socket.getPort() + " <-- " + XML);

								XMLReader(XML, socket);
							}
						});

					}
				});

				btnInterfacelevelsavailablerequest.setEnabled(true);

				btnAuthenticateRequest.setEnabled(true);
				btnInterfacelevelrequest.setEnabled(true);
				btnObsoleteInterfacelevelrequest.setEnabled(true);
				btnUnsupportedInterfacelevelrequest.setEnabled(true);

				generateXML();

			} else {
				dialog("Already connected", SWT.ICON_WARNING);
			}
		} catch (Exception ec) {
			tableEventAddLine("Error connectiong to server:" + ec);
			disconnect();
		}

	}

	private void buttonInterfacesLevelAvailableRequest_Click(SelectionEvent e) {

		tableInterfaceLevels.removeAll();
		sendXML("interfaceLevelsAvailableRequest");

	}

	private void buttonInterfacesLevelRequest_Click(SelectionEvent e) {

		if (textDeviceToken.getText().equals("")) {

			tableDeviceList.removeAll();

			if (tableInterfaceLevels.getSelectionCount() > 0) {

				//
				for (TableItem selectedItem : tableInterfaceLevels.getSelection()) {

					InterfaceLevel = selectedItem.getText(0);
					XSDLocalPath = selectedItem.getText(1);
					XSDVersion = selectedItem.getText(2);

				}

				sendXML("interfaceLevelRequest");

				tabFolder.setSelection(2);

			} else {

				dialog("Please select Interface Level", SWT.ICON_INFORMATION | SWT.OK);

			}

		} else {
			dialog("Already authenticated", SWT.ICON_WARNING | SWT.OK);
		}
	}

	private void buttonObsoleteInterfacesLevelRequest_Click(SelectionEvent e) {

		InterfaceLevel = "01.00";
		XSDLocalPath = "01.00";
		XSDVersion = "01.00";

		// send message
		sendXML("interfaceLevelRequest");

	}

	private void btnUnsuportedInterfacelevelrequest_Click(SelectionEvent e) {
		InterfaceLevel = "01.02";
		XSDLocalPath = "01.02";
		XSDVersion = "01.02";

		// send message
		sendXML("interfaceLevelRequest");
	}

	private void btnAuthenticateRequest_Click(SelectionEvent e) {

		// if (textDeviceToken.getText().equals("")) {
		tableDeviceList.removeAll();

		// send message
		sendXML("authenticateRequest");
		/*
		 * } else { dialog("Already authenticated", SWT.ICON_WARNING | SWT.OK);
		 * 
		 * }
		 */
	}

	private void btnCuptestInstancesRequest_Click(SelectionEvent e) {

		// send message
		sendXML("cuptestInstancesRequest");
	}

	private void btnByeRequest_Click(SelectionEvent e) {
		if (!textDeviceToken.getText().equals("")) {

			// send message
			sendXML("byeRequest");

		} else {
			dialog("InterfaceLevel value not found", SWT.ICON_WARNING | SWT.OK);
		}
	}

	private void btnDisconnect_Click(SelectionEvent e) {
		disconnect();
		tabFolder.setSelection(0);

	}

	private void sendXML(String XML) {

		generateXML(XML);
		// send message
		SendMsg(styledTextXML.getText());
	}

	private void SendMsg(String XML) {
		SendMsg(XML, platformSocket);
	}

	private void SendMsg(String XML, CupptSocket socket) {
		try {

			if (XML.contains("{MESSAGEID}"))
				socket.incrementMessageID();

			// replace XML vars
			XML = replace(XML, "{MESSAGEID}", String.valueOf(socket.getMessageID()));
			XML = replace(XML, "{XSDVersion}", XSDVersion);
			XML = replace(XML, "{INTERFACELEVEL}", InterfaceLevel);
			XML = replace(XML, "{AIRLINE}", AirlineCode);
			XML = replace(XML, "{PLATFORMDEFINEDPARAMETER}", "");

			XML = replace(XML, "{APPLICATIONCODE}", ApplicationCode);
			XML = replace(XML, "{APPLICATIONNAME}", ApplicationName);
			XML = replace(XML, "{APPLICATIONVERSION}", ApplicationVersion);
			XML = replace(XML, "{APPLICATIONDATA}", ApplicationData);

			// send header msg
			XML = generateMsgHeader(XML) + XML;

			tableEventAddLine("Message " + socket.getIP() + ":" + socket.getPort() + " --> " + XML);

			styledTextEventAppend('S', XML);

			socket.send(XML);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String generateMsgHeader(String XML) throws UnsupportedEncodingException {
		// send header msg
		String version = "01";
		String validCode = "00";
		String size = "" + Integer.toHexString(XML.getBytes("utf-8").length);

		while (size.length() < 6)
			size = "0" + size;

		return version + validCode + size.toUpperCase();
	}

	private void clear() {

		styledTextXML.setText("");
		styledTextEvent.setText("");
		styledTextXMLValidate.setText("");
		styledTextByeResponse.setText("");
		textDeviceToken.setText("");

		tableInterfaceLevels.removeAll();
		tableDeviceList.removeAll();

		btnInterfacelevelsavailablerequest.setEnabled(false);
		btnInterfacelevelrequest.setEnabled(false);
		btnUnsupportedInterfacelevelrequest.setEnabled(false);
		btnObsoleteInterfacelevelrequest.setEnabled(false);
		btnAuthenticateRequest.setEnabled(false);

		btnCuptestInstancesRequest.setEnabled(false);

	}

	/*
	 * Close the Input/Output streams and disconnect not much to do in the catch
	 * clause
	 */
	private void disconnect() {
		try {

			platformSocket.disconnect();

			clear();

		} catch (Exception e) {
			System.out.println("exception");

		} // not much else I can do

	}

	private int dialog(String Message, int OPTIONS) {

		// ICON_ERROR, ICON_INFORMATION, ICON_QUESTION, ICON_WARNING,
		// ICON_WORKING
		MessageBox dialog = new MessageBox(shlCupptSdk, OPTIONS);

		dialog.setText("Airline Application");
		dialog.setMessage(Message);

		return dialog.open();

	}

	private void tableEventAddLine(String text) {

		TableItem item = new TableItem(tableEvents, SWT.NONE);
		item.setText(0, getDate());
		item.setText(1, text);

		tableEvents.select(tableEvents.getItemCount() - 1);
		tableEvents.showSelection();

		System.out.println(text);
	}

	private void styledTextEventAppend(char T, String str) {

		String symbol = (T == 'S') ? "-->" : "<--";

		styledTextEvent.append(T + " " + symbol + " " + getDate() + " :\n");
		styledTextEvent.append(str);
		styledTextEvent.append("\n\r");
	}

	// donne la date avec un format par défaut
	private String getDate() {
		return getDate("dd/MM/yyyy HH:mm:ss.SSS");
	}

	// surcharge de la fonction précédente avec le choix du format
	private String getDate(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(new Date());
	}

	private void generateXML() {

		String type = "interfaceLevelsAvailableRequest";
		generateXML(type);
	}

	private void generateXML(String type) {
		// header
		String XML = "<cupps messageID=\"{MESSAGEID}\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" messageName=\""
				+ type + "\">";

		if (type.equals("interfaceLevelsAvailableRequest")) {
			XML += "<interfaceLevelsAvailableRequest hsXSDVersion = \"{XSDVersion}\"/>";
		} else if (type.equals("interfaceLevelRequest")) {
			XML += "<interfaceLevelRequest level=\"{INTERFACELEVEL}\"/>";
		} else if (type.equals("authenticateRequest")) {
			XML += "<authenticateRequest airline=\"{AIRLINE}\" eventToken=\"{APPLICATIONCODE}\" platformDefinedParameter=\"{PLATFORMDEFINEDPARAMETER}\" >";

			XML += "<applicationList>";
			XML += "<application applicationName=\"{APPLICATIONNAME}\" applicationVersion=\"{APPLICATIONVERSION}\" applicationData=\"{APPLICATIONDATA}\"/>";
			XML += "</applicationList>";

			XML += "</authenticateRequest>";
		} else if (type.equals("cuptestInstancesRequest")) {
			XML += "<cuptestInstancesRequest />";
		} else if (type.equals("byeRequest")) {
			XML += "<byeRequest />";
		}

		// end
		XML += "</cupps>";

		styledTextXML.setText(XML);

	}

	private String replace(String originalText, String subStringToFind,
			String subStringToReplaceWith) {
		int s = 0;
		int e = 0;

		StringBuffer newText = new StringBuffer();

		while ((e = originalText.indexOf(subStringToFind, s)) >= 0) {

			newText.append(originalText.substring(s, e));
			newText.append(subStringToReplaceWith);
			s = e + subStringToFind.length();

		}

		newText.append(originalText.substring(s));
		return newText.toString();

	}

	private void XMLReader(String rmessage, CupptSocket socket) {

		int startIndex = rmessage.indexOf('<');

		if (startIndex > -1) {
			try {

				// String prefix = rmessage.substring(0, startIndex);
				// System.out.println("prefix : '" + prefix + "'");

				String XML = rmessage.substring(rmessage.indexOf('<'));

				styledTextXMLValidate.setText("");
				// parse an XML document into a DOM tree
				DocumentBuilder parser;
				parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();

				// Document document = parser.parse(new File("instance.xml"));
				Document document = parser.parse(new InputSource(new StringReader(XML)));

				// show parsed message
				for (int i = 0; i < document.getElementsByTagName("*").getLength(); i++) {

					Node node = document.getElementsByTagName("*").item(i);

					if (node.getNodeType() == Node.ELEMENT_NODE) {

						styledTextXMLValidate.append(node.getNodeName() + "\n");

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								Node attr = attrs.item(j);
								styledTextXMLValidate.append("\t" + attr.getNodeName() + ": "
										+ attr.getNodeValue() + "\n");
							}
						}
					}
				}

				Node node = document.getElementsByTagName("cupps").item(0);
				String message = node.getAttributes().getNamedItem("messageName").getNodeValue();

				if (message.equals("interfaceLevelsAvailableResponse")) {

					tabFolder.setSelection(1);

					NodeList nodeList = document.getElementsByTagName("interfaceLevel");

					for (int i = 0; i < nodeList.getLength(); i++) {
						node = nodeList.item(i);

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							TableItem item = new TableItem(tableInterfaceLevels, SWT.NONE);

							item.setText(0, attrs.getNamedItem("level").getNodeValue());
							item.setText(1, attrs.getNamedItem("wsLocalPath").getNodeValue());
							item.setText(2, attrs.getNamedItem("xsdVersion").getNodeValue());

						}

					}
				} else if (message.equals("interfaceLevelResponse")) {

				} else if (message.equals("authenticateResponse")) {

					node = document.getElementsByTagName("authenticateResponse").item(0);

					if (node.hasAttributes()) {
						textDeviceToken.setText(node.getAttributes().getNamedItem("deviceToken")
								.getNodeValue());
					}

					NodeList nodeList = document.getElementsByTagName("device");

					if (InterfaceLevel.contains("00.")) {
						System.out.println("CUPPT Interface"); // CUPPT
																// interface

						for (int i = 0; i < nodeList.getLength(); i++) {
							node = nodeList.item(i);
							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								TableItem item = new TableItem(tableDeviceList, SWT.NONE);

								item.setText(0, attrs.getNamedItem("deviceName").getNodeValue());
								item.setText(1, attrs.getNamedItem("hostName").getNodeValue());
								item.setText(2, attrs.getNamedItem("IP").getNodeValue());
								item.setText(3, attrs.getNamedItem("Port").getNodeValue());
								item.setText(4, attrs.getNamedItem("Vendor").getNodeValue());
								item.setText(5, attrs.getNamedItem("Model").getNodeValue());
								item.setText(6, attrs.getNamedItem("Status").getNodeValue());

							}
						}

					} else {
						System.out.println("CUPPS Interface"); // CUPPS
																// Interface

						for (int i = 0; i < nodeList.getLength(); i++) {

							TableItem item = new TableItem(tableDeviceList, SWT.NONE);

							node = nodeList.item(i);

							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								item.setText(0, attrs.getNamedItem("deviceName").getNodeValue());

							}

							NodeList children = node.getChildNodes();

							for (int j = 0; j < children.getLength(); j++) {
								Node child = children.item(j);
								System.out.println("child nodeName : " + child.getNodeName());

								if (child.getNodeName().contains("DeviceParameter")) {
									NodeList subchildren = child.getChildNodes();

									for (int k = 0; k < subchildren.getLength(); k++) {
										Node subChild = subchildren.item(k);
										System.out.println("subChild nodeName : "
												+ subChild.getNodeName());

										if (subChild.getNodeName() == "ipAndPort") {
											NamedNodeMap attrs = subChild.getAttributes();

											item.setText(1, attrs.getNamedItem("hostName")
													.getNodeValue());
											item.setText(2, attrs.getNamedItem("ip").getNodeValue());
											item.setText(3, attrs.getNamedItem("port")
													.getNodeValue());

										}

										if (subChild.getNodeName() == "vendorModelInfo") {
											NamedNodeMap attrs = subChild.getAttributes();

											item.setText(4, attrs.getNamedItem("vendor")
													.getNodeValue());
											item.setText(5, attrs.getNamedItem("model")
													.getNodeValue());
										}

										if (subChild.getNodeName().contains("Status")) {
											NamedNodeMap attrs = subChild.getAttributes();

											for (int l = 0; l < attrs.getLength(); l++) {

												if (attrs.item(l).getNodeValue().equals("true")) {
													item.setText(6, attrs.item(l).getNodeName());
												}
											}
										}

									}

								}

							}
						}
					}

					colorizeDeviceStatus();

				} else if (message.equals("byeResponse")) {

					styledTextByeResponse.setText(XML);

					node = document.getElementsByTagName("byeResponse").item(0);
					NamedNodeMap attrs = node.getAttributes();

					if (attrs.getNamedItem("result").getNodeValue().equals("OK")) {
						btnDisconnect.setEnabled(true);
					} else {
						dialog("Query refused", SWT.ICON_ERROR);
					}

				} else if (message.equals("notify")) {

					String response = "notify \n";
					String textContent = "";

					NodeList nodeList = document.getElementsByTagName("notify").item(0)
							.getChildNodes();
					for (int i = 0; i < nodeList.getLength(); i++) {
						node = nodeList.item(i);

						response += "NodeName : " + node.getNodeName() + "\n";
						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {

								Node attr = attrs.item(j);
								response += "    " + attr.getNodeName();

								if (attr.getNodeName().length() <= 11) {
									response += "\t";
								}
								if (attr.getNodeName().length() <= 6) {
									response += "\t";
								}
								response += ": " + attr.getNodeValue() + "\n";
							}
						}

						if (!node.getTextContent().isEmpty()
								& !textContent.equals(node.getTextContent())) {
							response += "textContent : " + node.getTextContent() + "\n";
							textContent = node.getTextContent();

						}
					}

					dialog(response, SWT.ICON_WORKING);
				}
			}

			catch (ParserConfigurationException e1) {

				styledTextXMLValidate.setText("ParserConfigurationException : " + e1.getMessage());

			} catch (SAXParseException spe) {

				StringBuffer sb = new StringBuffer(spe.getMessage());
				sb.append("\n\tLine number: " + spe.getLineNumber());
				sb.append("\n\tColumn number: " + spe.getColumnNumber());

				styledTextXMLValidate.setText("SAXParseException : " + sb.toString());

			} catch (SAXException e1) {

				styledTextXMLValidate.setText("SAXException : " + e1.getMessage());

			} catch (IOException e1) {

				styledTextXMLValidate.setText("IOException : " + e1.getMessage());

			} catch (Exception e1) {

				styledTextXMLValidate.setText("Exception when extracting XML data");

			}
		}
	}

	private void colorizeDeviceStatus() {

		for (int i = 0; i < tableDeviceList.getItemCount(); i++) {
			TableItem item = tableDeviceList.getItem(i);

			if (item.getText(6).equalsIgnoreCase("ready")) {
				item.setForeground(6, green);
			} else if (item.getText(6).equalsIgnoreCase("init")) {
				item.setForeground(6, orange);
			} else {
				item.setForeground(6, red);
			}

		}
	}

}
